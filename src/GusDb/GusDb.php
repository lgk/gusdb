<?php
namespace GusDb;

use GuzzleHttp;
use Symfony\Component\DomCrawler\Crawler;

require_once __DIR__.'/deathbycaptcha.php';

class GusDb
{
    private static $dbyc_username;
    private static $dbyc_password;
    private $http;

    public function __construct($dbyc_username, $dbyc_password)
    {
        self::$dbyc_username = $dbyc_username;
        self::$dbyc_password = $dbyc_password;

        $this->http = new GuzzleHttp\Client;
        $this->http->setDefaultOption('cookies', true);
    }

    public function find($nip)
    {
        $this->http->get('http://stat.gov.pl/regon/');
        $image_binary = $this->http->get('http://stat.gov.pl/regon/Captcha.jpg?'.rand(111, 999))->getBody();
        $captcha = self::decodeCaptcha('base64:'.base64_encode($image_binary), self::$dbyc_username, self::$dbyc_password);

        $response = $this->http->post('http://stat.gov.pl/regon/', [
            'headers' => ['Set-Cookie' => "cCodeHistory=$captcha; criterion1TF=$nip; focusedObjIdGlobal=verifCodeTF; lastCCode=$captcha; isAmbiguousAnswer=false; toGenerNewVerifCode=false"],
            'body' => [
                'queryTypeRBSet' => '1nip',
                'criterion1TF' => $nip,
                'verifCodeTF' => $captcha,
                $captcha.'00' => '',
                $captcha.'11' => $nip
            ]
        ]);
        $response = $response->getBody();

        if (strpos($response, 'Nieprawidłowy kod weryfikacyjny') !== false) {
            throw new \RuntimeException('Nieprawidłowy kod weryfikacyjny');
        }
        if (strpos($response, 'Wprowadzony numer jest nieprawidłowy') !== false) {
            throw new \RuntimeException('Wprowadzony numer NIP jest nieprawidłowy');
        }
        if (strpos($response, 'nie występuje w rejestrze REGON') !== false) {
            throw new \RuntimeException('Numer NIP nie występuje w bazie GUS');
        }
        if (strpos($response, 'Podgląd wydruku') === false) {
            throw new \RuntimeException('Niezdefiniowany błąd');
        }

        $store = array();

        $crawler = new Crawler;
        $crawler->addHtmlContent($response);

        // pierwsza tabela
        $crawler->filter('table[id=dbResponse01T] tr')->each(function($node, $i) use(&$store) {
            $td = $node->filter('td');
            if ($td && $td->count() === 2) {
                $label = $td->eq(0)->text();
                $value = $td->eq(1)->text();
                // zamień dwie lub więcej spacji na jedną
                $store[$label] = preg_replace('#\s{2,}#', ' ', trim($value));
            }
        });

        // druga tabela
        $crawler->filter('table[id=dbResponse02T] tr')->each(function($node, $i) use(&$store) {
            $td = $node->filter('td');
            if ($td && $td->count() === 2) {
                $label = $td->eq(0)->text();
                $value = $td->eq(1)->text();
                // zamień dwie lub więcej spacji na jedną
                $store[$label] = preg_replace('#\s{2,}#', ' ', trim($value));
            }
        });

        if (count($store) < 8) {
            throw new \RuntimeException('Błąd parsowania');
        }

        $tmp = array();
        foreach (array_change_key_case($store, CASE_LOWER) as $key => $value) {
            $key = preg_replace('#\W+#u', '_', $key);
            $tmp[$key] = $value;
        }
        $store = $tmp;
        unset($tmp);

        $store['adres1'] = $store['poczta'];
        $store['adres2'] = $store['ulica_miejscowość'];
        $store['kod'] = substr($store['poczta'], 0, 6);
        $store['miejscowosc'] = substr($store['poczta'], 7);
        $store['regon'] = $store['numer_identyfikacyjny_regon'];
        $store['nip'] = $store['numer_nip'];
        $store['wojewodztwo'] = $store['województwo'];
        $store['firma'] = $store['nazwa'];

        if (isset($store['imię_imiona'])) {
            $store['imie'] = $store['imię_imiona'];
        }

        return $store;
    }

    private static function decodeCaptcha($captcha, $username, $password)
    {
        $dByc = new \DeathByCaptcha_HttpClient($username, $password);
        $result = $dByc->decode($captcha);
        if ($result) {
            return $result['text'];
        }
        return false;
    }
}
